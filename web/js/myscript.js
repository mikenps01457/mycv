$( function() {
    $( "#tabs" ).tabs();
	$(window).scroll(function() {
	   var hT = $('#page-center').offset().top,
		   hH = $('#page-center').outerHeight(),
		   wH = $(window).height(),
		   wS = $(this).scrollTop();
	   if (wS >= 300){
		$('#page-center .skill-bar').children().addClass('run');
	   }else{
		$('#page-center .skill-bar').children().removeClass('run'); 
	   }
	});
	/* create slide show */
	var htmlDefault = $('#slider').html();
	$('.sp').first().addClass('active');
	$('.sp').hide();    
	$('.active').show();
	$('.loading-mask').hide();
	$('.grid .detail-extensions .view-link').click(function(event){
		event.preventDefault();	
		var dtAttr = 'sp '+$(this).attr('data-slide');
		
		$('#slider .sp').each( function (index, element){			
			$(element).attr('class', dtAttr+'-'+index);
			if(index === 0){
				$(element).addClass('active');
			}
			if( $(element).hasClass('locator-0') ){
				var html = '<div class="sp locator-3" style="display: none"></div> <div class="sp locator-4" style="display: none"></div> <div class="sp locator-5" style="display: none"></div>';
				$(html).insertAfter( $('#slider .sp:nth-child(3n)') );
			}
			if( $(element).hasClass('login-0') ){
				var html = '<div class="sp login-3" style="display: none"></div> <div class="sp login-4" style="display: none"></div></div>';
				$(html).insertAfter( $('#slider .sp:nth-child(3n)') );
			} 
		});
		$('.loading-mask').show();
		$('#slider-wrapper ').show();
	});
	$('.loading-mask').click(function(){
		
		$('#slider').html( htmlDefault );
		$('#slider-wrapper ').hide();
		$('.loading-mask').hide();
	});
	
	$('#slider').on( 'click', '#button-next', function(){

		$('.active').removeClass('active').addClass('oldActive');    
		
		if ( $('.oldActive').next().is(':last-child')) {
			$('.sp').first().addClass('active');
		}
		else{ 
			$('.oldActive').next('.sp').addClass('active');
		}

		$('.oldActive').removeClass('oldActive');
		$('.sp').fadeOut();
		$('.active').fadeIn();
		
		
	});
	
	$('#slider ').on( 'click', '#button-previous', function(){
		$('.active').removeClass('active').addClass('oldActive');    
			if ( $('.oldActive').next().is(':first-child')) {
				$('.sp').last().addClass('active');
			}
		    else{
				$('.oldActive').prev('.sp').addClass('active');
			}
		$('.oldActive').removeClass('oldActive');
		$('.sp').fadeOut();
		$('.active').fadeIn();
	});	
	
	/* even translate */
	$('.vi').hide();
	$('#vi').click(function(){
		$('.vi').show();
		$('.en').hide();
	});
	$('#en').click(function(){
		$('.en').show();
		$('.vi').hide();
	})
});